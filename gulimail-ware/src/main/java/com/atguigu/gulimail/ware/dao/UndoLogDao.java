package com.atguigu.gulimail.ware.dao;

import com.atguigu.gulimail.ware.entity.UndoLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author Sheyi.Yan
 * @email 1465781407@qq.com
 * @date 2020-04-12 18:06:44
 */
@Mapper
public interface UndoLogDao extends BaseMapper<UndoLogEntity> {
	
}
