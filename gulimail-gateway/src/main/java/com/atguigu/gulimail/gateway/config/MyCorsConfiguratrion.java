package com.atguigu.gulimail.gateway.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsConfigurationSource;
import org.springframework.web.cors.reactive.CorsWebFilter;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;
import org.springframework.web.server.ServerWebExchange;

/**
 * @description: 解决跨域问题的顾虑器
 * @author: logic-yan
 * @createDate: 2020/4/17
 * @version: 1.0
 */
@Configuration
public class MyCorsConfiguratrion {

    @Bean
    public CorsWebFilter getCorsWebFilter(){
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        // 1.配置跨域
        config.addAllowedHeader("*");
        config.addAllowedMethod("*");
        config.addAllowedOrigin("*");//任意来源都允许
        config.setAllowCredentials(true);//是否允许携带Cooke跨域
        source.registerCorsConfiguration("/**",config);
        return new CorsWebFilter(source);
    }

}
