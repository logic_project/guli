package com.atguigu.gulimail.gateway;

import ch.qos.logback.core.db.DataSourceConnectionSource;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * 1.开启服务的注册与发现
 * 2.配置nacos的服务地址
 * 这个地方是处理引入common的时候引入了mybatis-plus自动配置数据源的错误，这个配置可以忽略掉数据源的配置
 * @author sheyi.yan
 */
@EnableDiscoveryClient
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class GulimailGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(GulimailGatewayApplication.class, args);
    }

}
