package io.renren.dao;

import org.apache.ibatis.annotations.Mapper;

/**
 * Oracle代码生成器
 *
 * @author 1465781407@qq.com
 * @since 2018-07-24
 */
@Mapper
public interface OracleGeneratorDao extends GeneratorDao {

}
