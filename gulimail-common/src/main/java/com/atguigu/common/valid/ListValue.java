package com.atguigu.common.valid;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 *
 * @author logic
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.PARAMETER, ElementType.TYPE_USE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(
        // 指定自己的注解校验器
        validatedBy = {ListValueConstraintValidator.class}
)
public @interface ListValue {

    String message() default "参数不在默认值内";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    int[] value() default { };


}
