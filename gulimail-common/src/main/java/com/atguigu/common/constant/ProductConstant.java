package com.atguigu.common.constant;

import lombok.Data;

/**
 * 商品服务的常量值
 * @author sheyi.yan
 */
public class ProductConstant {

    public enum AttrEnum{
        /**
         * 这是商品信息的基本属性
         */
        ATTR_TYPE_BASE(1,"基本属性"),ATTR_TYPE_SALE(0,"销售属性");
        private int code;
        private String msg;
        AttrEnum(int code, String msg) {
            this.code = code;
            this.msg = msg;
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }
    }

}
