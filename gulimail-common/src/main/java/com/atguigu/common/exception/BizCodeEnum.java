package com.atguigu.common.exception;

/**
 * 错误状态码的枚举
 * 错误编码的
 *  10. 表示通用
 *      001 参数格式校验失败
 *  11 商品错误消息
 *  12 订单
 *  13 购物车
 *  14 物流
 * @author sheyi.yan
 */
public enum BizCodeEnum {
    /**
     * 错误代码的code和详细信息
     *  10开通的是通用的消息
     */
    UNKNOW_EXCEPTION(10000,"系统未知异常"),
    VAILD_EXCEPTION(10001,"参数格式校验失败");

    private final Integer code;
    private final String msg;

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    BizCodeEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
