package com.atguigu.common.to;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 这个是服务之间调用的To
 * @author sheyi.yan
 */

@Data
public class SpuBoundTo {

    private Long spuId;
    private BigDecimal buyBounds;
    private BigDecimal growBounds;

}
