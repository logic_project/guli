package com.atguigu.gulimail.product.service.impl;

import com.atguigu.gulimail.product.entity.CategoryEntity;
import com.atguigu.gulimail.product.service.CategoryService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
class CategoryServiceImplTest {

    @Autowired
    CategoryService categoryService;

    @Test
    void queryPage() {
    }

    @Test
    void listWithTree() {
        List<CategoryEntity> entities = categoryService.listWithTree();
        entities.forEach(item->{
            System.out.println(item);
        });
    }
}