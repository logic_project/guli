package com.atguigu.gulimail.product.dao;

import com.atguigu.gulimail.product.entity.SkuImagesEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * sku图片
 * 
 * @author Sheyi.Yan
 * @email 1465781407@qq.com
 * @date 2020-05-22 15:38:54
 */
@Mapper
public interface SkuImagesDao extends BaseMapper<SkuImagesEntity> {
	
}
