package com.atguigu.gulimail.product.service;

import com.atguigu.gulimail.product.vo.AttrGroupRelationVo;
import com.atguigu.gulimail.product.vo.AttrResVo;
import com.atguigu.gulimail.product.vo.AttrVo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.gulimail.product.entity.AttrEntity;

import java.util.List;
import java.util.Map;

/**
 * 商品属性
 *
 * @author Sheyi.Yan
 * @email 1465781407@qq.com
 * @date 2020-04-12 12:22:14
 */
public interface AttrService extends IService<AttrEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 自定义新增对象
     * @param attr 试图对象
     */
    void saveAttr(AttrVo attr);

    /**
     * 获取分页数据
     * @param params 查询参数
     * @param catelogId 特殊的查询参数，如过是0，表示是查询全部，如果不为0，则表示的是根据 catelogId 去查询
     * @param type 查询类型
     * @return 分页数据
     */
    PageUtils queryBaseAttrPage(Map<String, Object> params, Long catelogId, String type);

    /**
     * 根据ID获取一个Vo对象
     * @param attrId id
     * @return vo对象
     */
    AttrResVo getAttrInfo(Long attrId);

    /**
     * 修改数据库信息
     * @param attrVo 这个地方使用的是vo对象
     */
    void updateAttr(AttrVo attrVo);

    /**
     * 根据分组ID查询所有的基本属性
     * @param attrgroupId 根据ID
     * @return 数据集合
     */
    List<AttrEntity> getRelationAttr(Long attrgroupId);

    /**
     * 批量删除关联关系
     * @param vos 批量删除
     */
    void deleteRelation(AttrGroupRelationVo... vos);

    /**
     * 获取当前分组没有关联的属性
     * @param attrgroupId 分组id
     * @param params 分页参数
     * @return 没有关联的属性的集合
     */
    PageUtils getNoRelationAttr(Map<String, Object> params, Long attrgroupId);
}

