package com.atguigu.gulimail.product.feign;

import com.atguigu.common.to.SkuReductionTo;
import com.atguigu.common.to.SpuBoundTo;
import com.atguigu.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient("gulimail-coupon")
public interface CouponFeignService {
    /**
     * 远程调用接口，进行数据的保存
     * @param spuBoundTo 数据集合
     * @return 是否成功的状态
     */
    @PostMapping("coupon/spubounds/save")
    R saveSpuBounds(@RequestBody SpuBoundTo spuBoundTo);

    /**
     * 远程调用接口
     * @param skuReductionTo 数据即可
     * @return 是否成功的状态
     */
    @PostMapping("coupon/skufullreduction/saveinfo")
    R saveSkuReduction(@RequestBody SkuReductionTo skuReductionTo);
}
