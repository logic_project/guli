package com.atguigu.gulimail.product.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 设置定返回的VO
 * @author sheyi.yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class AttrResVo extends AttrVo {

    /**
     * "catelogName": "手机/数码/手机", //所属分类名字
     * 			"groupName": "主体", //所属分组名字
     */
    private String catelogName;

    /**
     * 所属分组名字
     */
    private String groupName;

    /**
     * 三级分类的路径
     */
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private Long[] catelogPath;

}
