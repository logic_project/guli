package com.atguigu.gulimail.product;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * 1.整合mybatis-plus
 *  1).导入jar包
 *      <dependency>
 *             <groupId>com.baomidou</groupId>
 *             <artifactId>mybatis-plus-boot-starter</artifactId>
 *             <version>3.2.0</version>
 *      </dependency>
 *  2）.配置
 *      a.配置数据源
 *          a).导入数据驱动
 *          b).在配置文件配置数据源信息
 *      b.配置mybatis-plus
 *
 * 2.逻辑删除
 *      给相应的bean的属性上面加上@TableLogic注解
 *
 * 3.JSR303 校验
 *      1） 给类的属性上面添加注解java.validation.constraints,并定义自己需要的提示
 *      2） 给Controller方法里面添加@Valid注解，这样数据校验错误的时候后有默认的响应
 *      3） 给校验的bean后面紧跟一个BindingResult，就可以获取到校验结果
 *      4） 分组校验
 *      4） 自定义的注解解析器
 *
 * 4.统一的异常处理
 * RestControllerAdvice
 *  1)、编写统一的异常处理类，使用的注解是 @RestControllerAdvice
 *  2)、使用@ExceptionHandler这个注解来捕获对应的异常
 *
 */
@EnableFeignClients(basePackages = "com.atguigu.gulimail.product.feign")
@MapperScan("com.atguigu.gulimail.product.dao")
@EnableDiscoveryClient
@SpringBootApplication
public class GulimailProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(GulimailProductApplication.class, args);
    }

}
