package com.atguigu.gulimail.product.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 删除用的vo
 * @author sheyi.yan
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AttrGroupRelationVo {
    private Long attrId;
    private Long attrGroupId;
}
