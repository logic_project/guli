package com.atguigu.gulimail.product.vo;

import lombok.Data;

/**
 * 品牌
 * @author sheyi.yan
 */
@Data
public class BrandVo {

    /**
     * 品牌id
     */
    private Long brandId;
    /**
     * 品牌分类名称
     */
    private String brandName;

}
