package com.atguigu.gulimail.product.dao;

import com.atguigu.gulimail.product.entity.SpuInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * spu信息
 * 
 * @author Sheyi.Yan
 * @email 1465781407@qq.com
 * @date 2020-04-12 12:22:14
 */
@Mapper
public interface SpuInfoDao extends BaseMapper<SpuInfoEntity> {
	
}
