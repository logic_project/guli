package com.atguigu.gulimail.member.dao;

import com.atguigu.gulimail.member.entity.MemberLevelEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员等级
 * 
 * @author Sheyi.Yan
 * @email 1465781407@qq.com
 * @date 2020-04-12 18:03:03
 */
@Mapper
public interface MemberLevelDao extends BaseMapper<MemberLevelEntity> {
	
}
